﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    enum Color
    {
        None,
        White,
        Black
    }

    static class ColorMethods
    {
        public static Color FlipColor(this Color color)
        {
            if (color == Color.Black) return Color.White;
            if (color == Color.White) return Color.Black;
            return Color.None;
        }
    }
}
