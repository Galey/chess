﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    enum Figure
    {
        None,
        WhiteKing = 'K',
        WhiteQueen = 'Q',
        WhiteBishop = 'B',
        WhiteKnight = 'N',
        WhitePawn = 'P',

        BlackKing = 'k',
        BlackQueen = 'q',
        BlackBishop = 'b',
        BlackKnight = 'n',
        BlackPawn = 'p'
    }
}
